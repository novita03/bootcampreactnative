import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  StatusBar 
} from 'react-native';

// import LoginScreen from './Tugas/Tugas13/LoginScreen';
// import Main from './Tugas/Tugas14/components/Main';
import Index from './Tugas/Tugas15/index';
import IndexQuiz from './Quiz/Quiz3/index';

export default class App extends Component{
  render() {
    return (
        // <LoginScreen/>
        // <Main/> 
        // <Index/>
        <IdexQuiz />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
