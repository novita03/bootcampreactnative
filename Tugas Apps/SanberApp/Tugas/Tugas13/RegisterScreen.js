import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity, ScrollView, FlatList, TextInput, Button
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class RegisterScreen extends Component {
  render() {
  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={styles.logoSection}>
          <Image style={{width: 500, height: 150}} source={require('./images/logosanber.png')} />
        </View>
        <View styles={styles.titleSection}>
          <Text style={styles.labelInput}>Register</Text>
        </View>
        <View style={styles.formSection}>
          <Text style={styles.labelInput}>Username</Text>
          <TextInput style={styles.inputTextForm}/>

          <Text style={styles.labelInput}>Email</Text>
          <TextInput style={styles.inputTextForm}/>

          <Text style={styles.labelInput}>Password </Text>
          <TextInput style={styles.inputTextForm}/>

          <Text style={styles.labelInput}>Ulangi Password </Text>
          <TextInput style={styles.inputTextForm}/>

          <View style={styles.buttonSection}/>
            <Button title='Masuk' color="#3EC6FF" style={{borderRadius:20}} />
            <Text style={{textAlign:'center', marginBottom:20,marginTop:20}}>atau</Text>
            <Button title='Daftar' color="#003366" />
          </View>
        </View>
      </View>
    </ScrollView>
    );
} 
}
const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#FFFFFF',
  },
  logoSection :{
    top:63,
    alignItems:'center',
    marginBottom:50,
  },
  titleSection:{
    alignItems:'center',
    top: 100,
    height: 30,
  },
  titleScreen : {
    color: '#003366',
    fonSize: 24,
    fontFamily: 'Roboto',
  },
  formSection:{
    top:130,
    color: '#003366',
    fontSize:16,
    fontFamily:'Roboto',
    flexDirection:'column',
  },
  labelInput:{
    left:41,
    marginBottom:5,
  },
  inputTextForm:{
    height:40,
    borderColor:'grey',
    borderWidth:1,
    width:294,
    left:41,
    marginBottom:30,
  },
  buttonStyle:{
    borderRadius:16,
    width:10,
  }
});

