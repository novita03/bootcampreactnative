import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const profile ={
  name: 'Novita Krisma Diarti',
  position: 'Developer',
  gitlab: '@novita03',
  github: '@novita03',
  facebook: 'novita',
  instagram: '@novita03',
  twitter: '@novita03'
};

export default class AboutScreen extends Component {
  render() {
    return (
      <ScrollView>
        <View style ={styles.container}>
          <View style={styles.topSection}></View>
          <View style={styles.middleSection}>
            <Text style={styles.titleDetail}>Portofolio</Text>
            <View style={styles.line}></View>
          </View>
          <View style={styles.buttomSection}>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  topSection:{
    alignItems: 'center',
    top: 64,
    flexDirection: 'column'
  },
  title:{
    fonSize:36,
    color: '#003366'
  },
  profilePicture:{
    marginTop:20
  },
  detailProfile:{
    bottom:115,
    flexDirection:'column',
    alignItems: 'center'
  },
  name:{
    fontSize: 18,
    fontWeight:'bold',
    color:'#3EC6F'
  },
  position:{
    fontSize:18,
    fontWeight:bold,
    color:'#3EC6FF'
  },
  middleSection:{
    backgroundColor: '#EFEFEF',
    height:140,
    width:335,
    bottom:30,
    borderRadius:10,
    left:20,
    paddingHorizontal:15,
    paddingVertical:5
  }
});